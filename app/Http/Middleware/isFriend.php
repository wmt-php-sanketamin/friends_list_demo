<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Models\User;

use Session;


class isFriend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // $account_id = $request->route()->parameter('friend');
        $account_id = $request->friend;
        // dd($account_id);

        $user = $request->user()->id;
        $friends = User::with('friends')->where('id',$account_id)->first();

        if($friends == null){
            Session::flash('message', 'requsted user not found...!!!'); 
            return redirect('/friends');
        }

        if(!($friends->friends->contains('id', $user))) {
            Session::flash('message', 'You are Not Friend With ' .$friends->name . '...!!!'); 
            return redirect('/friends');
        }
        return $next($request);
    }
}
