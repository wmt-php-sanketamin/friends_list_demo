@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row d-flex justify-content-center">
            <div class="col-lg-5 col-md-6 col-sm-7">
                @if(Session::has('message'))
                    <p class="alert alert-danger }}">{{ Session::get('message') }}</p>
                @endif
                
                <div class="card">
                    <div class="img1"><img src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg" alt="..." ></div>
                    <div class="img2"><img src="../{{$friends->img_path}}" alt=""></div>
                    <div class="main-text p-3">
                        <h2 class="text-center">{{$friends->name}}</h2>
                        <h5 class="text-center">{{$friends->email}}</h5>
                        <div class="pt-2">
                            <h3>About:</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                        </div>
                        <div>
                            <h3>My Friends:</h3>
                            <div class="row pr-3 pl-3 pt-3 d-flex flex-row flex-wrap">
                                @forelse ($friends->friends as $friend)
                                    <div class="col-3 pb-3 d-flex flex-column text-center">
                                        <a class="text-dark"href="{{route('friends.show',$friend->id)}}">
                                            <div class="img3"><img src="../{{$friend->img_path}}" alt=""></div>
                                            <div>{{$friend->name}}</div>
                                        </a>
                                    </div>
                                @empty
                                    <p>No Friends.</p>
                                @endforelse
                            </div>    
                        </div>
                    </div>
                    <div class="socials pb-3 pr-3 pl-3 d-flex flex-row justify-content-around align-items-center">
                        <div><i class="fa fa-facebook fa-3x"></i></div>
                        <div><i class="fa fa-instagram fa-3x"></i></div>
                        <div><i class="fa fa-twitter fa-3x"></i></div>
                        <div><i class="fab fa-linkedin-in fa-3x"></i></div>
                        <div><i class="fab fa-audible fa-3x"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@yield('footer')
