@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-lg-5 col-md-6 col-sm-7">
            <h2>{{$user->name}}'s Friend</h2>
            <div class="card">
                <div class="img1"><img src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg" alt="..." ></div>
                <div class="img2"><img src="../{{$mutual_f->img_path}}" alt=""></div>
                <div class="main-text p-3">
                    <h2 class="text-center">{{$mutual_f->name}}</h2>
                    <h5 class="text-center">{{$mutual_f->email}}</h5>
                    <div class="pt-2">
                        <h3>About:</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                    </div>
                    <div>
                        <h3>Mutual Friends:</h3>
                        <div class="row pr-3 pl-3 pt-3 d-flex flex-row flex-wrap">
                            @forelse ($mutual_f->friends as $mutual)
                                <div class="col-3 d-flex flex-column text-center">
                                    <div class="img3"><img src="../{{$mutual->img_path}}" alt=""></div>
                                    <div>{{$mutual->name}}</div>
                                </div>
                            @empty
                                <p>No Mutual Friends.</p>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="socials pb-3 pr-3 pl-3 d-flex flex-row justify-content-around align-items-center">
                        <div><i class="fa fa-facebook fa-3x"></i></div>
                        <div><i class="fa fa-instagram fa-3x"></i></div>
                        <div><i class="fa fa-twitter fa-3x"></i></div>
                        <div><i class="fab fa-linkedin-in fa-3x"></i></div>
                        <div><i class="fab fa-audible fa-3x"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
   

@endsection


@yield('footer')
