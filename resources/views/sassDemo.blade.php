@extends('layouts.app')

@section('content')


    <h2>Sass Demo</h2>

    <p class="danger">Warning! This is Dange text.</p>

    <p class="safe">Warning! This is Safe text.</p>

    <button class="button button-report">Report</button>
    <button class="button-submit">Submit</button>
    
    <div class="margin-small">
        Small Margin
    </div>
    <div class="margin-normal">
        Normal Margin
    </div>
    <div class="margin-large">
        large Margin
    </div>
    
    
    <div class="test">
        <div class="facebook">facebook</div>
        <div class="twitter">Twitter</div>
        <div class="google_plus">google_plus</div>
        <div class="linked_in">linked_in</div>
    </div>

    
    <div> 
        <a class="test1" href="">
            Hello
            <div class="test1">
                World
            </div>
        </a>
    </div>

    <div class="b">
        <div class="test1">Hello World</div>
    </div>
@endsection 

@yield('footer')
